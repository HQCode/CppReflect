#include <stdio.h>
#include "KClassFactory.h"
#include "CKBaseClass.h"
#include "CKHelloClass.h"

int main()
{
	CKBaseClass *pVar = (CKBaseClass*)CKClassFactory::sharedClassFactory().getClassByName("CKHelloClass");
	if (!pVar)
	{
		return 0;
	}
	pVar->registProperty();

	int pValue = 123456;

	pVar->m_propertyMap["setm_pValue"](pVar, &pValue);
	pVar->display();

	getchar();
	return 0;
}
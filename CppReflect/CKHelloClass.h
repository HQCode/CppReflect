#include "KDynamicClass.h"
#include "CKBaseClass.h"
#include <map>
#include <iostream>
using namespace std;

class CKHelloClass:public CKBaseClass
{
private:
	string CKHelloClassName;
	static CKDynamicClass* m_CKHelloClassdc;
SYNTHESIZE(CKHelloClass, int*, m_pValue)

	CKHelloClass() {}
	virtual ~CKHelloClass(){}
	static void* createInstance()
	{
		return new CKHelloClass();
	}
	virtual void registProperty()override
	{
		m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue));
	}
	virtual void display() override
	{
		cout << *getm_pValue() << endl;
	}
protected:
	int *m_pValue;

};
IMPLEMENT_CLASS(CKHelloClass)
#ifndef CKBASECLASS_H
#define	CKBASECLASS_H
#include "KDynamicClass.h"

class CKBaseClass
{
private:
	DECLARE_CLASS(CKBaseClass)
public:
	CKBaseClass() {}
	virtual ~CKBaseClass() {}
	static void* createInstance() { return new CKBaseClass(); }
	virtual void registProperty() {}
	virtual void display() {}

	typedef void(*setValue)(CKBaseClass *t, void* c);
	map<string, setValue> m_propertyMap;
};



#define SYNTHESIZE(classType, varType, varName)    \
public:                                             \
	inline static void set##varName(CKBaseClass*cp, void*value){\
		classType* tp = (classType*)cp;                        \
		tp->varName = (varType)value;                      \
	}\
	inline varType get##varName(void) const {\
	return varName;\
	}

//CKDynamicClass* CKBaseClass::m_CKBaseClassdc = new CKDynamicClass("CKBaseClass", CKBaseClass::createInstance);

/*
#include "KDynamicClass.h"

class CKBaseClass;
typedef void(*setValue)(CKBaseClass *t, void* c);

class CKBaseClass
{
private:
	DECLARE_CLASS(CKBaseClass)

public:
	CKBaseClass() {}
	virtual ~CKBaseClass() {}
	static void* createInstance() { return new CKBaseClass(); }
	virtual void registProperty() {}
	virtual void display() {}
	map<string, setValue> m_propertyMap;
};



#define SYNTHESIZE(classType, varType, varName)    \
public:                                             \
	inline static void set##varName(CKBaseClass*cp, void*value){
\
	classType* tp = (classType*)cp;                        \
	tp->varName = (varType)value;                      \
	}\
	inline varType get##varName(void) const {
	\
	return varName; \
	}
	IMPLEMENT_CLASS(CKBaseClass)

	*/

#endif